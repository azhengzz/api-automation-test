# coding=utf-8

from app.cores.dictionaries import DISPATCHER_TYPE
from app.cores.dispatcher import AbstractLogicControllerDispatcher
from app.cores.logic_controller.logic_controller import (exec_simple_controller, exec_if_controller,
                                                         exec_loop_controller, exec_while_controller)


class SimpleControllerDispatcher(AbstractLogicControllerDispatcher):
    def __init__(self, logic_controller, recursive_func, dispatcher_type=DISPATCHER_TYPE.BUILD, logger=None,
                 dispatcher=None):
        super().__init__(logic_controller=logic_controller, recursive_func=recursive_func, logger=logger,
                         dispatcher=dispatcher, dispatcher_type=dispatcher_type)

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        self.dispatcher_logger.logger.info('[SIMPLE控制器]')
        exec_simple_controller(self.recursive_func, self.logic_controller.specific_controller)

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()


class IfControllerDispatcher(AbstractLogicControllerDispatcher):
    def __init__(self, logic_controller, recursive_func, dispatcher_type=DISPATCHER_TYPE.BUILD, logger=None,
                 dispatcher=None):
        super().__init__(logic_controller=logic_controller, recursive_func=recursive_func, logger=logger,
                         dispatcher=dispatcher, dispatcher_type=dispatcher_type)

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        self.dispatcher_logger.logger.info('[IF控制器][表达式内容:%s]' % self.logic_controller.specific_controller.expression)
        exec_if_controller(self.recursive_func, self.logic_controller.specific_controller)

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()


class LoopControllerDispatcher(AbstractLogicControllerDispatcher):
    def __init__(self, logic_controller, recursive_func, dispatcher_type=DISPATCHER_TYPE.BUILD, logger=None,
                 dispatcher=None):
        super().__init__(logic_controller=logic_controller, recursive_func=recursive_func, logger=logger,
                         dispatcher=dispatcher, dispatcher_type=dispatcher_type)

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        self.dispatcher_logger.logger.info('[LOOP控制器][循环次数:%s]' % self.logic_controller.specific_controller.expression)
        exec_loop_controller(self.recursive_func, self.logic_controller.specific_controller)

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()


class WhileControllerDispatcher(AbstractLogicControllerDispatcher):
    def __init__(self, logic_controller, recursive_func, dispatcher_type=DISPATCHER_TYPE.BUILD, logger=None,
                 dispatcher=None):
        super().__init__(logic_controller=logic_controller, recursive_func=recursive_func, logger=logger,
                         dispatcher=dispatcher, dispatcher_type=dispatcher_type)

    def set_up(self):
        super().set_up()

    def execute(self):
        super().execute()
        self.dispatcher_logger.logger.info('[WHILE控制器][表达式内容:%s]' % self.logic_controller.specific_controller.expression)
        exec_while_controller(self.recursive_func, self.logic_controller.specific_controller)

    def tear_down(self):
        super().tear_down()

    def run(self):
        super().run()
