# coding=utf-8
# 邮件通知

from typing import List, Mapping, Any
from flask import current_app, render_template

from app.email import send_email


def send_email_report(addresses: List, report_data: Mapping[str, Any]):
    """
    邮件发送本次调度结果（同步发送）
    :param addresses: 收件人地址
    :param report_data: 案例执行结果
    :return: None
    """
    send_email(
        subject="[ApiAutomationTest] 构建结果通知",
        sender=current_app.config['ADMINS'][0],
        recipients=addresses,
        # text_body=render_template("email/email_authentication.txt"),
        html_body=render_template("email/summary_report.html",
                                  report_data=report_data,
                                  module_results=report_data.get('module_results'),),
        async_send=False,
    )