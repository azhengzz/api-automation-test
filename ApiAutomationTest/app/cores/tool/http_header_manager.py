# coding=utf-8
from flask import session

from app.cores.case.http.http_header_pool_manager import HTTPHeaderPoolManager


class HTTPHeaderManager:

    def __init__(self, tool, dispatcher_logger, project_id=None):
        """
        初始化HTTP请求头工具对象
        :param tool: 工具
        :type tool: Tool
        :param dispatcher_logger: 日志
        :type dispatcher_logger: DispatcherLogger
        :param project_id: 工具所在项目id
        :type project_id: int
        """
        self.http_header_manager_tool = tool.specific_tool
        self.dispatcher_logger = dispatcher_logger
        if project_id is None:
            self.project_id = session.get('project_id')
        else:
            self.project_id = project_id

    def exec_tool(self):
        self.dispatcher_logger.logger.info('[HTTP请求头管理器][执行]')
        variables = HTTPHeaderPoolManager.get_http_header(project_id=self.project_id)
        for row in self.http_header_manager_tool.http_header_manager_list:
            variables[row.name_] = row.value_
