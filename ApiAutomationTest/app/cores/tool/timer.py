# coding=utf-8

from time import sleep


class Timer:

    def __init__(self, tool, dispatcher_logger):
        """
        初始化定时器对象
        :param tool: 定时器数据库对象
        :type tool: Tool
        :param dispatcher_logger: 调度日志
        :type dispatcher_logger: DispatcherLogger
        """
        self.timer_tool = tool.specific_tool
        self.dispatcher_logger = dispatcher_logger

    def exec_tool(self):
        self.dispatcher_logger.logger.info('[定时器][暂停%sms]' % self.timer_tool.delay)
        sleep(int(self.timer_tool.delay) / 1000)
