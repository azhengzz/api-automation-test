# coding=utf-8


class STATUS:
    """状态"""
    DELETED = '已删除'
    NORMAL = '正常'
    FORBIDDEN = '禁止'


class DISPATCHER_STATUS:
    """调度状态"""
    RUNNING = '正在运行'
    STOPPING = '正在停止'
    STOPPED = '已停止'
    FINISHED = '已完成'


class DISPATCHER_TYPE:
    """调度触发类型"""
    DEBUG = 'DEBUG'  # 单一案例组件执行
    BUILD = 'BUILD'  # 由模块或项目执行


class DISPATCHER_END_TYPE:
    """调度终止类型"""
    SUCCESS = '成功'
    ERROR = '错误'
    ABORT = '中止'  # 手动终止测试


class REPORT_RESULT:
    """报告结果"""
    SUCCESS = '成功'
    FAILURE = '失败'
    ERROR = '错误'
    SKIP = '跳过'
    ABORT = '中止'
    RUNNING = '运行中'


class TEST_FIELD:
    """测试字段"""
    TEXT_RESPONSE = '响应文本'
    RESPONSE_CODE = '响应码'
    RESPONSE_HEADERS = '响应头'
    REQUEST_DATA = '请求数据'
    REQUEST_HEADERS = '请求头'


class MATCHING_RULE:
    """匹配规则"""
    CONTAINS = '包括'
    MATCHES = '匹配'
    EQUALS = '相等'
    SUBSTRING = '子字符串'
    XPATH = 'XPath'


class EXPECTATION_LOGIC:
    """期望结果逻辑"""
    AND = '与'
    OR = '或'


class CASE_TYPE:
    """案例组件类型"""
    HTTP = 'HTTP'
    SQL = 'SQL'
    SSH = 'SSH'
    DEBUG = 'DEBUG'


class ELEMENT_TYPE:
    """元素类型"""
    PROJECT = 'PROJECT'
    MODULE = 'MODULE'
    SCENE = 'SCENE'
    CASE = 'CASE'
    LOGIC_CONTROLLER = 'LOGIC_CONTROLLER'
    TOOL = 'TOOL'


class LOGIC_CONTROLLER_TYPE:
    """控制器类型"""
    SCENE_CONTROLLER = 'SCENE'
    SIMPLE_CONTROLLER = 'SIMPLE'
    IF_CONTROLLER = 'IF'
    LOOP_CONTROLLER = 'LOOP'
    WHILE_CONTROLLER = 'WHILE'


class DB_TYPE:
    """数据库类型"""
    MYSQL = 'MYSQL'
    ORACLE = 'ORACLE'


class TOOL_TYPE:
    """工具组件类型"""
    TIMER = 'TIMER'
    VARIABLE_DEFINITION = 'VARIABLE_DEFINITION'
    SCRIPT = 'SCRIPT'
    HTTP_HEADER_MANAGER = 'HTTP_HEADER_MANAGER'
    HTTP_COOKIE_MANAGER = 'HTTP_COOKIE_MANAGER'


class CONTENT_TYPE:
    """HTTP请求体数据类型"""
    X_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded'
    FORM_DATA = 'multipart/form-data'


class DISPATCHER_TRIGGER_TYPE:
    """调度触发类型"""
    BY_HAND = '手工'
    BY_SCHEDULE = '定时任务'
