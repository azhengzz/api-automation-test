$(document).ready(function () {

    // 项目表格实例化
    let table_module_list = new getModuleListTable();
    table_module_list.init();

    // 绑定事件
    $btn_add_module.on('click', function(){
        $modal_add_module.modal('show');
    });
    $btn_save_add_module.on('click', addModule);
    $btn_save_modify_module.on('click', modifyModule);
    $btn_start_project_test.on('click', startProjectTest);
    $btn_stop_project_test.on('click', stopProjectTest);

    // 查询项目调度信息
    getProjectDispatcherInfo()
});

let $btn_start_project_test = $('#btn-start-project-test');
let $btn_stop_project_test = $('#btn-stop-project-test');
let $table_module_list = $('#table-module-list');
let $modal_add_module = $('#modal-add-module');
let $modal_modify_module = $('#modal-modify-module');
let $btn_add_module = $('#btn-add-module');
let $input_project_id = $('#input-project-id');
let $input_add_module_name = $('#input-add-module-name');
let $input_add_module_description = $('#input-add-module-description');
let $input_modify_module_id = $('#input-add-module-id');
let $input_modify_module_name = $('#input-modify-module-name');
let $input_modify_module_description = $('#input-modify-module-description');
let $btn_save_add_module = $('#btn-save-add-module');
let $btn_save_modify_module = $('#btn-save-modify-module');
let dispatcher_id = null; // 项目调度id 打开模块页面时查询返回调度id，或在调度启动主推拿到调度id

// 调度开始
user_socket.on('DISPATCHER_BEGIN', function (data) {
    dispatcher_id = data.id;
    if (data.type === 'project'){
        projectDispatcherRunning();
        // message("项目测试启动成功", "success");
        message(`<a class="text-left" target="_blank" href='/report/detail/${data.report_id}' style="text-decoration:underline;">项目测试启动成功，点击跳转到编号${data.report_id}报告</a>`, "success");
    }
});
// 调度结束
user_socket.on('DISPATCHER_END', function (data) {
    dispatcher_id = null;
    if (data.type === 'project'){
        if (data.end_type === '成功'){
            message("项目测试执行完成", "success");
        }else if (data.end_type === '错误'){
            message("项目测试执行出现错误, 请查看结果日志", "error");
        }else if (data.end_type === '终止'){
            message("项目测试执行已中止", "success");
        }else{
            message("项目测试执行结束", "warning");
        }        projectDispatcherStoppedFinished();
    }
});

function getModuleListTable() {
    let table = new Object;

    table.init = function () {
        $table_module_list.bootstrapTable({
            buttonsClass: 'primary',            // 定义按钮类
            cardView: false,                    // 是否显示详细视图
            cache: false,                       // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性
            contentType: 'application/x-www-form-urlencoded',  // 请求参数的内容类型 默认是'application/json'
            // checkboxHeader: false,              // 展示check-all按钮
            classes: 'table table-bordered table-hover table-striped table-borderless',  // 图表样式
            clickToSelect: false,               // 是否启用点击选中行
            detailView: false,                  // 是否显示父子表
            // height: 500,                        // 行高，如果没有设置height属性，表格自动根据记录条数决定表格高度
            locale: 'zh-CN',
            method: 'post',                     // 请求方式
            minimumCountColumns: 2,             // 最少允许的列数
            pageList: [5, 10, 25, 50, 100],     // 可供选择的每页的行数
            pageNumber:1,                       // 初始化加载第一页，默认第一页
            pageSize: 5,                        // 每页的记录行数
            pagination: true,                   // 是否显示分页
            paginationLoop: true,               // 循环翻页模式
            queryParams: queryParams,           // 查询参数
            reorderableRows: true,              // 调整行位置
            search: true,                       // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            // sortable: true,                     // 是否启用排序
            // sortOrder: 'asc',                   // 排序方式
            sortClass: 'table-active',          // 排序列样式
            sortReset: true,                    // 重置被排序的列（点击三次）
            strictSearch: false,                // 严格搜索，非模糊搜素
            striped: true,                      // 是否显示行间隔色
            // searchHighlight: true,              // bug?
            showColumns: true,                  // 是否显示所有的列
            showColumnsToggleAll: true,         // 全选开关
            showPaginationSwitch: true,         // 展示是否分页开关
            showRefresh: true,                  // 是否显示刷新按钮
            showToggle: true,                    // 是否显示详细视图和列表视图的切换按钮
            sidePagination: 'client',           // 分页方式：client客户端分页，server服务端分页
            toolbar: '#table-module-toolbar',   // 工具按钮用哪个容器
            toolbarAlign: 'left',               // 工具栏按钮位置
            uniqueId: 'id',                     // 每一行的唯一标识，一般为主键列
            url: '/ajax/module/get',            // 请求后台的URL
            useRowAttrFunc: true,               // 和reorderableRows配合使用，true表示会触发拖拽事件
            // virtualScroll: true,
            columns: [
                // {
                //     checkbox: true
                // },
                {
                    field: 'id',
                    title: '编号',
                }, {
                    field: 'name',
                    title: '模块名称',
                    class: 'cursor-pointer',
                }, {
                    field: 'description',
                    title: '模块描述',
                    class: 'cursor-pointer',
                }, {
                    field: 'creator',
                    title: '创建者',
                    sortable: true,
                    class: 'cursor-pointer',
                }, {
                    field: 'create_time',
                    title: '创建时间',
                    sortable: true,
                    class: 'cursor-pointer',
                }, {
                    field: 'last_updater',
                    title: '最后更新者',
                    class: 'cursor-pointer',
                }, {
                    field: 'last_updated_time',
                    title: '最后更新时间',
                    sortable: true,
                    class: 'cursor-pointer',
                }, {
                    filed: 'action',
                    title: '操作',
                    formatter: actionFormatter,
                    events: 'actionEvents',
                }
            ],
            // 事件
            onClickRow: onClickRow,
            onReorderRow: onReorderRow,
        });
    };

    function actionFormatter(value, row, index, field) {
        return '<button class="btn btn-outline-secondary btn-delete-row mr-2">删除</button>' +
            '<button class="btn btn-outline-secondary btn-modify-row">修改</button>'
    }

    function queryParams(params) {
        return {
            project_id: $input_project_id.val(),
        }
    }

    function onClickRow(row, $element, field){
        if (field === 7 || field === 'id'){  // 如果是点击了操作或编号列则不跳转
            return;
        }
        window.location.href="/scene?module_id=" + row.id;
    }

    function onReorderRow () {
        let newAllData = $table_module_list.bootstrapTable('getData');
        $table_module_list.bootstrapTable('load', newAllData);
        // 调整后台数据
        orderModule(newAllData);
    }

    window.actionEvents = {
        'click .btn-delete-row': function (event, value, row, index) {
            Swal.fire({
                title: '确定删除 ' + row.name + ' 模块吗？',
                text: "删除后将无法恢复!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: '确定',
                cancelButtonColor: '#d33',
                cancelButtonText: '取消',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/module/delete',
                        data: {
                            id: row.id,
                            project_id: $input_project_id.val(),
                        },
                        success: function (data, status, xhr) {
                            if (data.error_no === 0){
                                message("测试模块删除成功", "success");
                                $table_module_list.bootstrapTable('refresh');
                            }else{
                                message("测试模块删除失败: " + data.error_msg, "error");
                            }
                        },
                    });
                }
            });
        },

        'click .btn-modify-row': function (event, value, row, index) {
            $modal_modify_module.modal('show');
            $input_modify_module_id.val(row.id);
            $input_modify_module_name.val(row.name);
            $input_modify_module_description.val(row.description);
        }
    };

    return table;
}

function addModule() {
    let name = $input_add_module_name.val();
    let description = $input_add_module_description.val();
    let project_id = $input_project_id.val();

    $.ajax({
        type: 'POST',
        url: '/ajax/module/add',
        data: {
            name: name,
            description: description,
            project_id: project_id,
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                message("测试模块新增成功", "success");
                $modal_add_module.modal('hide');
                $table_module_list.bootstrapTable('refresh');
            }else{
                message("测试模块新增失败: " + data.error_msg, "error");
            }
        },
    });

}

function modifyModule() {
    let name = $input_modify_module_name.val();
    let description = $input_modify_module_description.val();
    let id = $input_modify_module_id.val();

    $.ajax({
        type: 'POST',
        url: '/ajax/module/modify',
        data: {
            id: id,
            name: name,
            description: description,
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                message("项目修改成功", "success");
                $modal_modify_module.modal('hide');
                $table_module_list.bootstrapTable('refresh');
            }else{
                message("项目修改失败: " + data.error_msg, "error");
            }
        },
    });
}

function orderModule(data) {
    $.ajax({
        type: 'POST',
        url: '/ajax/module/order',
        data: {
            data: JSON.stringify(data),
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
            }else{
                message("模块顺序调整失败: " + data.error_msg, "error");
            }
        },
    });
}

// 执行当前模块测试
function startProjectTest() {
    $.ajax({
        type: 'POST',
        url: '/ajax/project/test/start',
        data: {
            project_id: $input_project_id.val(),
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                // message("模块测试启动成功");
                // $btn_start_project_test.attr('disabled', true);
                // $btn_stop_project_test.attr('disabled', '');
            }else{
                message("项目测试启动失败: " + data.error_msg, "error");
                projectDispatcherStoppedFinished();
            }
        }
    });
    projectDispatcherStarting();
}

// 终止当前模块测试
function stopProjectTest() {
    $.ajax({
        type: 'POST',
        url: '/ajax/project/test/stop',
        data: {
            project_id: $input_project_id.val(),
            dispatcher_id: dispatcher_id,
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
            }else{
                message("项目测试终止失败: " + data.error_msg, "error");
            }
        }
    });
    projectDispatcherStopping();
}

// 查询项目调度信息
function getProjectDispatcherInfo() {
    $.ajax({
        type: 'POST',
        url: '/ajax/project/test/get',
        data: {
            project_id: $input_project_id.val(),
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                if (data.status === '正在运行') {
                    dispatcher_id = data.dispatcher_id;
                    projectDispatcherRunning();
                }else if (data.status === '正在停止'){
                    projectDispatcherStopping();
                }else if (data.status === '已停止' || data.status === '已完成'){
                    projectDispatcherStoppedFinished();
                }
            }else{
                message("项目测试数据查询失败: " + data.error_msg, "info");
                projectDispatcherStoppedFinished();
            }
        },
    });
}

// 项目调度不同状态-正在启动 运行中 已停止/已完成 正在停止 缺省状态
function projectDispatcherStarting() {
    $btn_start_project_test.text('正在启动');
    $btn_start_project_test.attr('disabled', true);
    $btn_start_project_test.append("<span class=\"spinner-grow spinner-grow-sm\"></span>")
    $btn_stop_project_test.removeAttr('disabled');
}
function projectDispatcherRunning() {
    $btn_start_project_test.text('正在执行');
    $btn_start_project_test.attr('disabled', true);
    $btn_start_project_test.append("<span class=\"spinner-grow spinner-grow-sm\"></span>")
    $btn_stop_project_test.removeAttr('disabled');
}
function projectDispatcherStoppedFinished() {
    $btn_start_project_test.text('执行测试');
    $btn_stop_project_test.text('终止测试');
    $btn_start_project_test.removeAttr('disabled');
    $btn_stop_project_test.attr('disabled', true);
}
function projectDispatcherStopping() {
    $btn_start_project_test.text('执行测试');
    $btn_start_project_test.attr('disabled', true);
    $btn_stop_project_test.text('正在终止');
    $btn_stop_project_test.append("<span class=\"spinner-grow spinner-grow-sm\"></span>")
}
