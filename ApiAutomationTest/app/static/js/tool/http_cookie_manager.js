function getHTTPCookieManagerToolElement(tool_id) {
    let element = new Object;
    element.dom = new Object;
    element.obj = new Object;

    // 案例页面元素
    element.dom.$input_tool_name = $(`#input-tool-name-${tool_id}`);
    element.dom.$input_tool_description = $(`#input-tool-description-${tool_id}`);
    element.dom.$btn_tool_save = $(`#btn-tool-save-${tool_id}`);
    element.dom.$div_navigation_tool_name = $(`.tool-name[data-tool-id=${tool_id}]`);

    // 对象
    element.obj.table_http_cookie_manager = null;  // 变量定义表格

    // 初始化
    element.init = function(data_variables) {
        // 事件绑定
        eventBinding();
        // 渲染HTTPCookie管理器表格
        renderHTTPCookieManagerTable(data_variables);
    };

    // 事件绑定
    function eventBinding() {
        element.dom.$input_tool_name.on('change keyup', handleKeyUpAndChange);
        element.dom.$btn_tool_save.on('click', saveTool);

        function handleKeyUpAndChange(){
            let tool_name = element.dom.$input_tool_name.val();
            element.dom.$div_navigation_tool_name.children().text(tool_name);
        }
    }

    // 渲染HTTPCookie管理器表格
    function renderHTTPCookieManagerTable(data_attributes) {
        const option_table_http_cookie_manager = {
            data: data_attributes,
            rowHeaders: true,
            // colHeaders: true,
            licenseKey: 'non-commercial-and-evaluation',
            // 表格宽度
            // width: '95vw',  // 不指定宽度使其自适应容器
            // 拉伸方式
            stretchH: 'all',
            // tab键自动换行切换
            autoWrapRow: true,
            height: '65vh',
            // 最大行数
            maxRows: 999,
            // 允许手工移动行或列
            manualRowResize: true,
            manualColumnResize: true,
            // 列名
            colHeaders: [
                '名称',
                '值',
                '域',
                '路径',
                '安全',
            ],
            // 为列设置默认值
            dataSchema: {
                name: '',
                value: '',
                url_encode: false,
                content_type: 'text/plain',
                include_equals: true,
            },
            // 设置列数据类型
            columns: [
                {
                    data: 'name'
                },
                {
                    data: 'value',
                },
                {
                    data: 'domain',
                },
                {
                    data: 'path',
                },
                {
                    data: 'secure',
                    type: 'checkbox',
                },
            ],
            // 列宽比例
            colWidths: [2, 5, 5, 3, 1],
            manualRowMove: true,
            manualColumnMove: false,
            // 右键菜单
            contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'alignment', '---------', 'copy', 'cut'],
            // 列是否支持过滤
            filters: false,
            // 下拉菜单
            dropdownMenu: ['make_read_only', '---------', 'alignment'],
            // 语言
            language: 'zh-CN',
            // 是否允许无效数据 默认 true
            allowInvalid: false,
            afterPaste: afterPasteHookForTableParam,
        };
        let $container = $(`#table-http-cookie-manager-${tool_id}`);
        element.obj.table_http_cookie_manager = $container.handsontable(option_table_http_cookie_manager).handsontable('getInstance');

        // 解决复制粘贴后checkBox中的值由布尔类型变为字符串的问题
        function afterPasteHookForTableParam() {
            let data = element.obj.table_http_cookie_manager.getSourceData();
            for (let i = 0; i < data.length; ++i) {
                let row = data[i];
                if (row.secure === 'false'){
                    row.secure = false;
                }
                if (row.secure === 'true'){
                    row.secure = true;
                }
            }
            element.obj.table_http_cookie_manager.loadData(data);
        }
    }

    function saveTool() {
        let tool_name = element.dom.$input_tool_name.val();
        let tool_description = element.dom.$input_tool_description.val();
        $.ajax({
            type: 'POST',
            url: '/ajax/tool/http_cookie_manager/save',
            data: {
                tool_id: tool_id,
                tool_name: tool_name,
                tool_description: tool_description,
                attrs: JSON.stringify(element.obj.table_http_cookie_manager.getSourceData()),
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message('HTTPCookie保存成功', 'success');
                }else{
                    message("HTTPCookie保存失败: " + data.error_msg, "error");
                }
            }
        });
    }

    // 关键词查找
    element.mark = function (text) {
        // 匹配到则返回true
        let pat = new RegExp(text);
        let value = '';
        let markFlag = false;
        // 名称
        value = element.dom.$input_tool_name.val();
        if (pat.test(value)) return true;
        // 注释
        value = element.dom.$input_tool_description.val();
        if (pat.test(value)) return true;
        // 期望表格
        markFlag = false;
        value = element.obj.table_http_cookie_manager.getSourceData();
        $.each(value, function (index, row) {
            $.each(row, function (column, data) {
                if (pat.test(data)){
                    markFlag = true;
                    return false;
                }
            });
        });
        if (markFlag) return true;

        // 未匹配到返回false
        return false;
    };

    return element;
}