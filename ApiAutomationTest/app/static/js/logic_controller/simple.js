// $(document).ready(function () {
//
//     // 事件绑定
//     $(document).on('keydown', documentSaveSimpleController);
//     $input_simple_controller_name.on('change keyup', handleKeyUpAndChange);
//     $btn_simple_controller_save.on('click', saveSimpleController);
//
//     function handleKeyUpAndChange(){
//         let simple_controller_name = $input_simple_controller_name.val();
//         $wtd_div_logic_controller_content.children('.logic-controller-content-name').text(simple_controller_name);
//     }
//
// });
//
// let simple_controller_id = $('#input-simple-controller-id').val();
// let $input_logic_controller_id = $('#input-logic-controller-id').val();
// let $input_simple_controller_name = $('#input-simple-controller-name');
// let $btn_simple_controller_save = $('#btn-simple-controller-save');
//
// // top页面
// let wtd = window.top.document;
// let $wtd_div_logic_controller_content= $('.logic-controller-content[data-logic-controller-id='+ $input_logic_controller_id +']', wtd);
//
// // 保存测试场景数据
// function saveSimpleController() {
//     let simple_controller_name = $input_simple_controller_name.val();
//     $.ajax({
//         type: 'POST',
//         url: '/ajax/logic_controller/simple_controller/save',
//         data: {
//             simple_controller_id: simple_controller_id,
//             simple_controller_name: simple_controller_name,
//         },
//         success: function (data, status, xhr) {
//             if (data.error_no === 0){
//                 message('Simple控制器保存成功', 'success');
//             }else{
//                 message("Simple控制器保存失败: " + data.error_msg, "error");
//             }
//         }
//     });
// }
//
// // ctrl+s保存测试场景数据
// function documentSaveSimpleController(event) {
//     //可以判断是不是mac，如果是mac,ctrl变为花键
//     if (event.keyCode == 83 && (navigator.platform.match("Mac") ? event.metaKey : event.ctrlKey)) {
//         event.preventDefault();  // 阻止元素事件的默认行为
//         saveSimpleController();
//     }
// }

// 获取简单控制器组件对象
function getSimpleControllerElement(logic_controller_id) {

    let element = new Object;
    element.dom = new Object;  // 保存组件中的元素Dom对象

    // 元素
    element.dom.$input_logic_controller_name = $(`#input-logic-controller-name-${logic_controller_id}`);
    element.dom.$input_logic_controller_description = $(`#input-logic-controller-description-${logic_controller_id}`);
    element.dom.$btn_logic_controller_save = $(`#btn-logic-controller-save-${logic_controller_id}`);
    element.dom.$wtd_div_logic_controller_content= $(`.logic-controller-content[data-logic-controller-id=${logic_controller_id}]`);

    // 初始化
    element.init = function() {
        eventBinding();
    };

    // 事件绑定
    function eventBinding() {
        element.dom.$input_logic_controller_name.on('change keyup', handleKeyUpAndChange);
        element.dom.$btn_logic_controller_save.on('click', saveLogicController);

        function handleKeyUpAndChange(){
            let logic_controller_name = element.dom.$input_logic_controller_name.val();
            element.dom.$wtd_div_logic_controller_content.children('.logic-controller-content-name').text(logic_controller_name);
        }
    }

    // 保存
    function saveLogicController() {
        let logic_controller_name = element.dom.$input_logic_controller_name.val();
        let logic_controller_description = element.dom.$input_logic_controller_description.val();
        $.ajax({
            type: 'POST',
            url: '/ajax/logic_controller/simple_controller/save',
            data: {
                logic_controller_id: logic_controller_id,
                logic_controller_name: logic_controller_name,
                logic_controller_description: logic_controller_description,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message('Simple控制器保存成功', 'success');
                }else{
                    message("Simple控制器保存失败: " + data.error_msg, "error");
                }
            }
        });
    }

    // 关键词查找
    element.mark = function (text) {
        // 匹配到则返回true
        let pat = new RegExp(text);
        let value = '';
        // 名称
        value = element.dom.$input_logic_controller_name.val();
        if (pat.test(value)) return true;
        // 注释
        value = element.dom.$input_logic_controller_description.val();
        if (pat.test(value)) return true;

        // 未匹配到返回false
        return false;
    };

    return element;

}
