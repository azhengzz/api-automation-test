$(document).ready(function () {

    // 项目表格实例化
    let table_report_list = new getReportListTable();
    table_report_list.init();

});

let $table_report_list = $('#table-report-list');

function getReportListTable() {
    let table = new Object;

    table.init = function () {
        $table_report_list.bootstrapTable({
            buttonsClass: 'primary',            // 定义按钮类
            cardView: false,                    // 是否显示详细视图
            cache: false,                       // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性
            // checkboxHeader: false,              // 展示check-all按钮
            classes: 'table table-bordered table-hover table-striped table-borderless',  // 图表样式
            clickToSelect: false,               // 是否启用点击选中行
            detailView: false,                  // 是否显示父子表
            // height: 500,                        // 行高，如果没有设置height属性，表格自动根据记录条数决定表格高度
            locale: 'zh-CN',
            method: 'post',                     // 请求方式
            minimumCountColumns: 2,             // 最少允许的列数
            pageNumber:1,                       // 初始化加载第一页，默认第一页
            pagination: true,                   // 是否显示分页
            pageSize: 5,                        // 每页的记录行数
            pageList: [10, 25, 50, 100],        // 可供选择的每页的行数
            paginationLoop: true,               // 循环翻页模式
            // queryParams: table.queryParams,     //传递参数
            toolbar: '#table-report-toolbar',   // 工具按钮用哪个容器
            toolbarAlign: 'left',               // 工具栏按钮位置
            url: '/ajax/report/get',            // 请求后台的URL
            uniqueId: 'id',                     // 每一行的唯一标识，一般为主键列
            search: true,                       // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            // searchHighlight: true,              // bug?
            striped: true,                      // 是否显示行间隔色
            strictSearch: false,                // 严格搜索，非模糊搜素
            showColumns: true,                  // 是否显示所有的列
            showColumnsToggleAll: true,         // 全选开关
            showRefresh: true,                  // 是否显示刷新按钮
            showToggle: true,                   // 是否显示详细视图和列表视图的切换按钮
            sidePagination: 'client',           // 分页方式：client客户端分页，server服务端分页
            // sortable: true,                     //是否启用排序
            sortClass: 'table-active',          // 排序列样式
            // sortOrder: 'asc',                   // 排序方式
            sortReset: true,                    // 重置被排序的列（点击三次）
            columns: [
            // {
            //     checkbox: true
            // },
            {
                field: 'id',
                title: '编号',
                class: 'cursor-pointer',
            }, {
                field: 'name',
                title: '名称',
                class: 'cursor-pointer',
            }, {
                field: 'start_time',
                title: '开始时间',
                class: 'cursor-pointer',
            }, {
                field: 'end_time',
                title: '结束时间',
                class: 'cursor-pointer',
            }, {
                field: 'elapsed_time',
                title: '耗时',
                class: 'cursor-pointer',
            },{
                field: 'result',
                title: '结果',
                class: 'cursor-pointer',
                formatter: resultFormatter,
            },{
                filed: 'action',
                title: '操作',
                formatter: actionFormatter,
                events: 'actionEvents',
            }
            ],
            // 事件
            onClickRow: onClickRow,
        });
    };

    function actionFormatter(value, row, index, field) {
        return '<button class="btn btn-outline-secondary btn-delete-row">删除</button>'
    }

    function onClickRow(row, $element, field){
        if (field === 6){  // 如果是点击了操作列则不跳转
            return;
        }
        window.location.href="/report/detail/" + row.id;
    }

    function resultFormatter(value, row, index, field) {
        if(value === '失败' ){
            return '<h4>\n' +
                '   <span class="badge badge-danger">失败</span>\n' +
                '</h4>';
        }else if(value === '错误'){
            return '<h4>\n' +
                '   <span class="badge badge-error">错误</span>\n' +
                '</h4>';
        }else if(value === '成功') {
            return '<h4>\n' +
                '   <span class="badge badge-success">成功</span>\n' +
                '</h4>';
        }else if(value === '中止') {
            return '<h4>\n' +
                '   <span class="badge badge-secondary">终止</span>\n' +
                '</h4>';
        }else if(value === '运行中') {
            return '<h4>\n' +
                '   <span class="badge badge-primary">运行中</span>\n' +
                '</h4>';
        }else{
            return value;
        }
    }

    window.actionEvents = {
        'click .btn-delete-row': function (event, value, row, index) {
            Swal.fire({
                title: '确定删除 编号:' + row.id + ' 名称:' + row.name + ' 报告吗？',
                text: "删除后将无法恢复!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: '确定',
                cancelButtonColor: '#d33',
                cancelButtonText: '取消',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/report/delete',
                        data: {
                            id: row.id,
                        },
                        success: function (data, status, xhr) {
                            if (data.error_no === 0){
                                message("报告删除成功", "success");
                                $table_report_list.bootstrapTable('refresh');
                            }else{
                                message("报告删除失败: " + data.error_msg, "error");
                            }
                        },
                    });
                }
            });
        },
    };

    return table;
}
