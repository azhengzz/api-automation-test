// $(document).ready(function () {
//
//     // 输入框格式化
//     let maskOption = {
//         translation: {
//             'Z': {
//                 pattern: /./, optional: true
//             }
//         }
//     };
//     $('#input-host').mask('Z'.repeat(50), maskOption);
//     $('#input-port').mask('Z'.repeat(50), maskOption);
//     $('#input-connect-timeout').mask('Z'.repeat(50), maskOption);
//     // $("#input-port").inputmask({regex: "\\d{0,10}"});
//     // $('#input-connection-timeout').inputmask({regex: "\\d{0,10}"});
//     $('#input-user').mask('Z'.repeat(50), maskOption);
//     $('#input-password').mask('Z'.repeat(50), maskOption);
//     $('#input-charset').mask('Z'.repeat(50), maskOption);
//
//     // 渲染期望表格
//     renderExpectationTable();
//     // 支持上下分割
//     renderSplitter([50, 300]);
//
//     // sql脚本
//     ace_sql_editor = ace.edit("div-ace-sql");
//     ace_sql_editor.setTheme("ace/theme/eclipse");
//     ace_sql_editor.session.setMode("ace/mode/sql");
//
// });
//
// let ace_sql_editor = null;  // sql脚本编辑器
//
// // 界面元素
// let $input_host = $("#input-host");
// let $input_port = $("#input-port");
// let $input_connect_timeout = $("#input-connect-timeout");
// let $input_user = $("#input-user");
// let $input_password = $("#input-password");
// let $select_db_type = $("#select-db-type");
// let $input_charset = $("#input-charset");
//
// // 用例保存
// function saveCase() {
//     let id = $btn_case_save.data("case-id").toString();
//     let name = $input_case_name.val();
//     let description = $input_case_description.val();
//     let host = $input_host.val();
//     let port = $input_port.val();
//     let connect_timeout = $input_connect_timeout.val();
//     let user = $input_user.val();
//     let password = $input_password.val();
//     let sql = ace_sql_editor.session.getValue();
//     let db_type = $select_db_type.val();
//     let charset = $input_charset.val();
//     let preprocessor_script_py_str = ace_preprocessor_script_editor.session.getValue();
//     let postprocessor_script_py_str = ace_postprocessor_script_editor.session.getValue();
//
//     let expectation_logic = '';
//     if ($radio_expectation_logic_and.is(':checked')){
//         expectation_logic = '与';
//     }
//     if ($radio_expectation_logic_or.is(':checked')){
//         expectation_logic = '或';
//     }
//
//     $.ajax({
//         type: 'POST',
//         url: '/ajax/case/sql/save',
//         data: {
//             id: id,
//             name: name,
//             description: description,
//             host: host,
//             port: port,
//             connect_timeout: connect_timeout,
//             user: user,
//             password: password,
//             expectation_logic: expectation_logic,
//             expectation_json: JSON.stringify(table_expectation.getSourceData()),
//             sql: sql,
//             db_type: db_type,
//             charset: charset,
//             preprocessor_script: preprocessor_script_py_str,
//             postprocessor_script: postprocessor_script_py_str,
//         },
//         success: function (data, status, xhr) {
//             if (data.error_no === 0){
//                 message("保存案例成功", "success")
//             }else{
//                 message("保存案例失败: " + data.error_msg, "error");
//             }
//         },
//     });
// }


function getSQLCaseElement(case_id, case_type) {
    let element = getBaseCaseElement(case_id, case_type);
    let baseCaseInit = element.init;  // baseCase中定义的初始化函数
    let baseCaseRenderSplitter = element.renderSplitter;  // 分隔栏渲染
    let baseMark = element.mark;  // 关键词搜索

    // 界面元素
    element.dom.$input_host = $(`#input-host-${case_id}`);
    element.dom.$input_port = $(`#input-port-${case_id}`);
    element.dom.$input_connect_timeout = $(`#input-connect-timeout-${case_id}`);
    element.dom.$input_user = $(`#input-user-${case_id}`);
    element.dom.$input_password = $(`#input-password-${case_id}`);
    element.dom.$select_db_type = $(`#select-db-type-${case_id}`);
    element.dom.$input_charset = $(`#input-charset-${case_id}`);

    element.obj.ace_sql_editor = null;  // sql脚本编辑器
    element.obj.splitterMinSize = [50, 150];  // splitter分割条最小长度


    element.init = function (data_expectations) {
        // 参数
        // data_expectations: 案例期望数据

        // 支持上下分割
        // element.renderSplitter([50, 300]);
        // baseCase初始化
        baseCaseInit(data_expectations);
        // 输入框格式化
        maskInput();
        // 渲染文本编辑
        renderAceEditor();
    };

    // 输入框格式化
    function maskInput() {
        // 输入框格式化
        let maskOption = {
            translation: {
                'Z': {
                    pattern: /./, optional: true
                }
            }
        };
        element.dom.$input_host.mask('Z'.repeat(50), maskOption);
        element.dom.$input_port.mask('Z'.repeat(50), maskOption);
        element.dom.$input_connect_timeout.mask('Z'.repeat(50), maskOption);
        // $("#input-port").inputmask({regex: "\\d{0,10}"});
        // $('#input-connection-timeout').inputmask({regex: "\\d{0,10}"});
        element.dom.$input_user.mask('Z'.repeat(50), maskOption);
        element.dom.$input_password.mask('Z'.repeat(50), maskOption);
        element.dom.$input_charset.mask('Z'.repeat(50), maskOption);
    }

    // 渲染文本编辑
    function renderAceEditor() {
        // sql脚本
        element.obj.ace_sql_editor = ace.edit(`div-ace-sql-${case_id}`);
        element.obj.ace_sql_editor.setTheme("ace/theme/eclipse");
        element.obj.ace_sql_editor.session.setMode("ace/mode/sql");
    }

    // 用例保存
    element.saveCase = function() {
        let id = element.dom.$btn_case_save.data("case-id").toString();
        let name = element.dom.$input_case_name.val();
        let description = element.dom.$input_case_description.val();
        let host = element.dom.$input_host.val();
        let port = element.dom.$input_port.val();
        let connect_timeout = element.dom.$input_connect_timeout.val();
        let user = element.dom.$input_user.val();
        let password = element.dom.$input_password.val();
        let sql = element.obj.ace_sql_editor.session.getValue();
        let db_type = element.dom.$select_db_type.val();
        let charset = element.dom.$input_charset.val();
        let preprocessor_script_py_str = element.obj.ace_preprocessor_script_editor.session.getValue();
        let postprocessor_script_py_str = element.obj.ace_postprocessor_script_editor.session.getValue();

        let expectation_logic = '';
        if (element.dom.$radio_expectation_logic_and.is(':checked')){
            expectation_logic = '与';
        }
        if (element.dom.$radio_expectation_logic_or.is(':checked')){
            expectation_logic = '或';
        }

        $.ajax({
            type: 'POST',
            url: '/ajax/case/sql/save',
            data: {
                id: id,
                name: name,
                description: description,
                host: host,
                port: port,
                connect_timeout: connect_timeout,
                user: user,
                password: password,
                expectation_logic: expectation_logic,
                expectation_json: JSON.stringify(element.obj.table_expectation.getSourceData()),
                sql: sql,
                db_type: db_type,
                charset: charset,
                preprocessor_script: preprocessor_script_py_str,
                postprocessor_script: postprocessor_script_py_str,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message("保存案例成功", "success")
                }else{
                    message("保存案例失败: " + data.error_msg, "error");
                }
            },
        });
    };

    // 分隔栏
    element.renderSplitter = function () {
        baseCaseRenderSplitter(element.obj.splitterMinSize);
    };

    // 关键词搜索
    element.mark = function (text) {
        let result = baseMark(text);
        if (result) return true;
        // 匹配到则返回true
        let pat = new RegExp(text);
        let value = '';
        let markFlag = false;
        // 主机名或IP
        value = element.dom.$input_host.val();
        if (pat.test(value)) return true;
        // 端口号
        value = element.dom.$input_port.val();
        if (pat.test(value)) return true;
        // 超时时间
        value = element.dom.$input_connect_timeout.val();
        if (pat.test(value)) return true;
        // 字符集
        value = element.dom.$input_charset.val();
        if (pat.test(value)) return true;
        // 用户名
        value = element.dom.$input_user.val();
        if (pat.test(value)) return true;
        // 密码
        value = element.dom.$input_password.val();
        if (pat.test(value)) return true;
        // sql
        value = element.obj.ace_sql_editor.getValue();
        if (pat.test(value)) return true;
        // 未匹配到返回false
        return false;
    };

    return element;
}
