// $(document).ready(function () {
//
//     const option_table_cookie_manager = {
//         data: table_cookie_manager_data,
//         height: '20vw',
//         // width: '50vw',  // 不指定宽度使其自适应容器
//         licenseKey: 'non-commercial-and-evaluation',
//         // 列名
//         colHeaders: ["Name", "Value", "Domain", "Path", "Expires", 'Delete', 'secure', 'version', 'port', 'discard', 'comment', 'comment_url', 'rfc2109', 'rest'],
//         rowHeaders: true,
//         // 拉伸方式
//         stretchH: 'all',
//         // 列支持排序
//         columnSorting: true,
//         // 右键菜单
//         contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'alignment', '---------', 'copy', 'cut'],
//         columns: [
//             {data: 'name',},
//             {data: 'value',},
//             {data: 'domain',},
//             {data: 'path',},
//             {data: 'expires',},
//             {data: 'Delete', renderer: renderButtons},
//             {data: 'secure',},
//             {data: 'version',},
//             {data: 'port',},
//             {data: 'discard',},
//             {data: 'comment',},
//             {data: 'comment_url',},
//             {data: 'rfc2109',},
//             {data: 'rest',},
//         ],
//         // 为列设置默认值
//         dataSchema: {
//             name: '',
//             value: '',
//             domain: null,
//             path: null,
//             expires: null,
//             secure: false,
//             version: 0,
//             port: null,
//             discard: true,
//             comment: null,
//             comment_url: null,
//             rfc2109: false,
//             rest: null,
//         },
//         // 列宽比例
//         colWidths: [2, 5, 2, 1, 1, 1],
//         // 隐藏某列
//         hiddenColumns: {
//             // set columns that are hidden by default
//             columns: [6,7,8,9,9,10,11,12,13],
//             // show where are hidden columns
//             indicators: false,
//         },
//         // 语言
//         language: 'zh-CN',
//     };
//
//     function renderButtons(instance, td, row, col, prop, value, cellProperties) {
//         function remove() {
//             instance.alter('remove_row', row);
//         }
//         td.innerHTML = "<button id='btn-remove-row' class='btn btn-info btn-sm mt-auto' type='button'>点击</button>";
//         $($(td).children("button")[0]).bind("click", remove);
//     }
//
//     const container_table_cookie_manager = document.getElementById('table-cookie-manager');
//     table_cookie_manager = new Handsontable(container_table_cookie_manager, option_table_cookie_manager);
//
// });

// let table_cookie_manager = null;
// let table_cookie_manager_data = null;
// let $btn_case_cookie = $("#btn-case-cookie");
// let $btn_save_cookie = $("#btn-save-cookie");
// let $modal_cookie_manager = $("#modal-cookie-manager");

// // 事件绑定
// $btn_case_cookie.bind("click", function () {
//     showCookie();
// });
// $btn_save_cookie.bind("click", function () {
//     saveCookie();
// });


// function showCookie() {
//     $modal_cookie_manager.modal('show');
//     $.ajax({
//         type: 'POST',
//         url: '/ajax/case/http/cookie/get',
//         data: {},
//         success: function (data, status, xhr) {
//             if (data.error_no === 0){
//                 table_cookie_manager.loadData(data.cookies);
//             }else{
//                 message("获取Cookie数据失败: " + data.error_msg, "error");
//             }
//         },
//     });
//
//     setTimeout(function () {  // 增加延时处理解决模态框打开时表格的渲染问题（可能没有渲染完全）
//         table_cookie_manager.render();
//     }, 200);
// }
//
//
// function saveCookie() {
//     $.ajax({
//         type: 'POST',
//         url: '/ajax/case/http/cookie/save',
//         data: {
//             cookies:JSON.stringify(table_cookie_manager.getSourceData()),
//         },
//         success: function (data, status, xhr) {
//             if (data.error_no === 0){
//                 message("更新Cookies数据成功", "success");
//             }else{
//                 message("更新Cookies数据失败: " + data.error_msg, "error");
//             }
//         },
//     });
// }


function getCookieManager(case_id) {

    let element = new Object;
    element.dom = new Object;
    element.obj = new Object;

    // 元素
    element.dom.$btn_case_cookie = $(`#btn-case-cookie-${case_id}`);
    element.dom.$btn_save_cookie = $(`#btn-save-cookie-${case_id}`);
    element.dom.$modal_cookie_manager = $(`#modal-cookie-manager-${case_id}`);

    element.obj.table_cookie_manager = null;
    element.obj.table_cookie_manager_data = null;

    element.init = function () {
        // 事件绑定
        eventBinding();
        // 渲染cookie表格
        renderCookieManagerTable();
    };

    // 事件绑定
    function eventBinding() {
        element.dom.$btn_case_cookie.bind("click", function () {
            showCookie();
        });
        element.dom.$btn_save_cookie.bind("click", function () {
            saveCookie();
        });

        function showCookie() {
            element.dom.$modal_cookie_manager.modal('show');
            $.ajax({
                type: 'POST',
                url: '/ajax/case/http/cookie/get',
                data: {},
                success: function (data, status, xhr) {
                    if (data.error_no === 0){
                        element.obj.table_cookie_manager.loadData(data.cookies);
                    }else{
                        message("获取Cookie数据失败: " + data.error_msg, "error");
                    }
                },
            });

            setTimeout(function () {  // 增加延时处理解决模态框打开时表格的渲染问题（可能没有渲染完全）
                element.obj.table_cookie_manager.render();
            }, 200);
        }

        function saveCookie() {
            $.ajax({
                type: 'POST',
                url: '/ajax/case/http/cookie/save',
                data: {
                    cookies:JSON.stringify(element.obj.table_cookie_manager.getSourceData()),
                },
                success: function (data, status, xhr) {
                    if (data.error_no === 0){
                        message("更新Cookies数据成功", "success");
                    }else{
                        message("更新Cookies数据失败: " + data.error_msg, "error");
                    }
                },
            });
        }


    }

    // 渲染cookie表格
    function renderCookieManagerTable(table_cookie_manager_data) {
        const option_table_cookie_manager = {
            data: table_cookie_manager_data,
            height: '20vw',
            // width: '50vw',  // 不指定宽度使其自适应容器
            licenseKey: 'non-commercial-and-evaluation',
            // 列名
            colHeaders: ["Name", "Value", "Domain", "Path", "Expires", 'Delete', 'secure', 'version', 'port', 'discard', 'comment', 'comment_url', 'rfc2109', 'rest'],
            rowHeaders: true,
            // 拉伸方式
            stretchH: 'all',
            // 列支持排序
            columnSorting: true,
            // 右键菜单
            contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'alignment', '---------', 'copy', 'cut'],
            columns: [
                {data: 'name',},
                {data: 'value',},
                {data: 'domain',},
                {data: 'path',},
                {data: 'expires',},
                {data: 'Delete', renderer: renderButtons},
                {data: 'secure',},
                {data: 'version',},
                {data: 'port',},
                {data: 'discard',},
                {data: 'comment',},
                {data: 'comment_url',},
                {data: 'rfc2109',},
                {data: 'rest',},
            ],
            // 为列设置默认值
            dataSchema: {
                name: '',
                value: '',
                domain: null,
                path: null,
                expires: null,
                secure: false,
                version: 0,
                port: null,
                discard: true,
                comment: null,
                comment_url: null,
                rfc2109: false,
                rest: null,
            },
            // 列宽比例
            colWidths: [2, 5, 2, 1, 1, 1],
            // 隐藏某列
            hiddenColumns: {
                // set columns that are hidden by default
                columns: [6,7,8,9,9,10,11,12,13],
                // show where are hidden columns
                indicators: false,
            },
            // 语言
            language: 'zh-CN',
        };

        function renderButtons(instance, td, row, col, prop, value, cellProperties) {
            function remove() {
                instance.alter('remove_row', row);
            }
            td.innerHTML = "<button id='btn-remove-row' class='btn btn-info btn-sm mt-auto' type='button'>点击</button>";
            $($(td).children("button")[0]).bind("click", remove);
        }

        const container_table_cookie_manager = document.getElementById(`table-cookie-manager-${case_id}`);
        element.obj.table_cookie_manager = new Handsontable(container_table_cookie_manager, option_table_cookie_manager);
    }

    return element;
}

