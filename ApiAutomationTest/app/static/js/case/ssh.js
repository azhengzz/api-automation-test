// $(document).ready(function () {
//
//     // 输入框格式化
//     let maskOption = {
//         translation: {
//             'Z': {
//                 pattern: /./, optional: true
//             }
//         }
//     };
//     $('#input-host-name').mask('Z'.repeat(50), maskOption);
//     $('#input-port').mask('Z'.repeat(50), maskOption);
//     $('#input-connection-timeout').mask('Z'.repeat(50), maskOption);
//     // $("#input-port").inputmask({regex: "\\d{0,10}"});
//     // $('#input-connection-timeout').inputmask({regex: "\\d{0,10}"});
//     $('#input-user-name').mask('Z'.repeat(50), maskOption);
//     $('#input-password').mask('Z'.repeat(50), maskOption);
//
//     // 渲染期望表格
//     renderExpectationTable();
//     // 支持上下分割
//     renderSplitter([50, 300]);
//
//     // ssh命令
//     ace_command_editor = ace.edit("div-ace-command");
//     ace_command_editor.setTheme("ace/theme/eclipse");
//     ace_command_editor.session.setMode("ace/mode/sh");
//
// });
//
// let ace_command_editor = null;  // shell命令编辑器
//
// // 界面元素
// let $input_host_name = $("#input-host-name");
// let $input_port = $("#input-port");
// let $input_connection_timeout = $("#input-connection-timeout");
// let $input_user_name = $("#input-user-name");
// let $input_password = $("#input-password");
//
// // 用例保存
// function saveCase() {
//     let id = $btn_case_save.data("case-id").toString();
//     let name = $input_case_name.val();
//     let description = $input_case_description.val();
//     let host_name = $input_host_name.val();
//     let port = $input_port.val();
//     let connection_timeout = $input_connection_timeout.val();
//     let user_name = $input_user_name.val();
//     let password = $input_password.val();
//     let command_shell_str = ace_command_editor.session.getValue();
//     let preprocessor_script_py_str = ace_preprocessor_script_editor.session.getValue();
//     let postprocessor_script_py_str = ace_postprocessor_script_editor.session.getValue();
//
//     let expectation_logic = '';
//     if ($radio_expectation_logic_and.is(':checked')){
//         expectation_logic = '与';
//     }
//     if ($radio_expectation_logic_or.is(':checked')){
//         expectation_logic = '或';
//     }
//
//     $.ajax({
//         type: 'POST',
//         url: '/ajax/case/ssh/save',
//         data: {
//             id: id,
//             name: name,
//             description: description,
//             host_name: host_name,
//             port: port,
//             connection_timeout: connection_timeout,
//             user_name: user_name,
//             password: password,
//             expectation_logic: expectation_logic,
//             expectation_json: JSON.stringify(table_expectation.getSourceData()),
//             command: command_shell_str,
//             preprocessor_script: preprocessor_script_py_str,
//             postprocessor_script: postprocessor_script_py_str,
//         },
//         success: function (data, status, xhr) {
//             if (data.error_no === 0){
//                 message("保存案例成功", "success")
//             }else{
//                 message("保存案例失败: " + data.error_msg, "error");
//             }
//         },
//     });
// }


function getSSHCaseElement(case_id, case_type) {
    let element = getBaseCaseElement(case_id, case_type);
    let baseCaseInit = element.init;  // baseCase中定义的初始化函数
    let baseCaseRenderSplitter = element.renderSplitter;  // 分隔栏渲染
    let baseMark = element.mark;  // 关键词搜索

    // 界面元素
    element.dom.$input_host_name = $(`#input-host-name-${case_id}`);
    element.dom.$input_port = $(`#input-port-${case_id}`);
    element.dom.$input_connection_timeout = $(`#input-connection-timeout-${case_id}`);
    element.dom.$input_user_name = $(`#input-user-name-${case_id}`);
    element.dom.$input_password = $(`#input-password-${case_id}`);

    element.obj.ace_command_editor = null;  // shell命令编辑器
    element.obj.splitterMinSize = [50, 150];  // splitter分割条最小长度

    element.init = function (data_expectations) {
        // 参数
        // data_expectations: 案例期望数据

        // 支持上下分割
        // element.renderSplitter([50, 300]);
        // baseCase初始化
        baseCaseInit(data_expectations);
        // 输入框格式化
        maskInput();
        // 渲染文本编辑
        renderAceEditor();
    };

    // 输入框格式化
    function maskInput() {
        let maskOption = {
            translation: {
                'Z': {
                    pattern: /./, optional: true
                }
            }
        };
        element.dom.$input_host_name.mask('Z'.repeat(50), maskOption);
        element.dom.$input_port.mask('Z'.repeat(50), maskOption);
        element.dom.$input_connection_timeout.mask('Z'.repeat(50), maskOption);
        // $("#input-port").inputmask({regex: "\\d{0,10}"});
        // $('#input-connection-timeout').inputmask({regex: "\\d{0,10}"});
        element.dom.$input_user_name.mask('Z'.repeat(50), maskOption);
        element.dom.$input_password.mask('Z'.repeat(50), maskOption);
    }

    // 渲染文本编辑
    function renderAceEditor() {
        // ssh命令
        element.obj.ace_command_editor = ace.edit(`div-ace-command-${case_id}`);
        element.obj.ace_command_editor.setTheme("ace/theme/eclipse");
        element.obj.ace_command_editor.session.setMode("ace/mode/sh");
    }

    // 用例保存
    element.saveCase = function() {
        let id = case_id;
        let name = element.dom.$input_case_name.val();
        let description = element.dom.$input_case_description.val();
        let host_name = element.dom.$input_host_name.val();
        let port = element.dom.$input_port.val();
        let connection_timeout = element.dom.$input_connection_timeout.val();
        let user_name = element.dom.$input_user_name.val();
        let password = element.dom.$input_password.val();
        let command_shell_str = element.obj.ace_command_editor.session.getValue();
        let preprocessor_script_py_str = element.obj.ace_preprocessor_script_editor.session.getValue();
        let postprocessor_script_py_str = element.obj.ace_postprocessor_script_editor.session.getValue();

        let expectation_logic = '';
        if (element.dom.$radio_expectation_logic_and.is(':checked')){
            expectation_logic = '与';
        }
        if (element.dom.$radio_expectation_logic_or.is(':checked')){
            expectation_logic = '或';
        }

        $.ajax({
            type: 'POST',
            url: '/ajax/case/ssh/save',
            data: {
                id: id,
                name: name,
                description: description,
                host_name: host_name,
                port: port,
                connection_timeout: connection_timeout,
                user_name: user_name,
                password: password,
                expectation_logic: expectation_logic,
                expectation_json: JSON.stringify(element.obj.table_expectation.getSourceData()),
                command: command_shell_str,
                preprocessor_script: preprocessor_script_py_str,
                postprocessor_script: postprocessor_script_py_str,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message("保存案例成功", "success")
                }else{
                    message("保存案例失败: " + data.error_msg, "error");
                }
            },
        });
    };

    // 分隔栏
    element.renderSplitter = function () {
        baseCaseRenderSplitter(element.obj.splitterMinSize);
    };

    // 关键词搜索
    element.mark = function (text) {
        let result = baseMark(text);
        if (result) return true;
        // 匹配到则返回true
        let pat = new RegExp(text);
        let value = '';
        let markFlag = false;
        // 主机名
        value = element.dom.$input_host_name.val();
        if (pat.test(value)) return true;
        // 端口
        value = element.dom.$input_port.val();
        if (pat.test(value)) return true;
        // 连接超时
        value = element.dom.$input_connection_timeout.val();
        if (pat.test(value)) return true;
        // 用户名
        value = element.dom.$input_user_name.val();
        if (pat.test(value)) return true;
        // 密码
        value = element.dom.$input_password.val();
        if (pat.test(value)) return true;
        // 命令
        value = element.obj.ace_command_editor.getValue();
        if (pat.test(value)) return true;
        // 未匹配到返回false
        return false;
    };

    return element;
}
