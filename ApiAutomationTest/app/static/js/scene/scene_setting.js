// $(document).ready(function () {
//     // 输入框格式化
//     // $input_scene_name.inputmask({regex: ".{0,200}"});
//     // $input_scene_description.inputmask({regex: ".{0,200}"});
//     let maskOption = {
//         translation: {
//             'Z': {
//                 pattern: /./, optional: true
//             }
//         }
//     };
//     $input_scene_name.mask('Z'.repeat(200), maskOption);  // 限制长度为200
//     $input_scene_description.mask('Z'.repeat(200), maskOption);  // 限制长度为200
//
//     // 事件绑定
//     $(document).on('keydown', documentSaveSceneSetting);
//     $input_scene_name.on('change keyup', function(){
//         let scene_name = $input_scene_name.val();
//         $wtd_div_scene_name.children('b').text(scene_name);
//     });
//     $btn_scene_setting_save.on('click', saveSceneSetting);
//
// });
//
// let scene_id = $('#input-scene-id').val();
// let $input_scene_name = $('#input-scene-name');
// let $input_scene_description = $('#input-scene-description');
// let $btn_scene_setting_save = $('#btn-scene-setting-save');
//
// // top页面
// let wtd = window.top.document;
// let $wtd_div_scene_name = $('.scene-name[data-scene-id='+ scene_id +']', wtd);

function getSceneElement(scene_id) {

    let element = new Object;
    element.dom = new Object;  // 保存组件中的元素Dom对象

    // 元素
    element.dom.$input_scene_name = $(`#input-scene-name-${scene_id}`);
    element.dom.$input_scene_description = $(`#input-scene-description-${scene_id}`);
    element.dom.$btn_scene_setting_save = $(`#btn-scene-setting-save-${scene_id}`);
    element.dom.$wtd_div_scene_name = $(`.scene-name[data-scene-id=${scene_id}]`);  // TODO rename $wtd_div_scene_name

    // // 输入框格式化
    // maskInput();
    // // 事件绑定
    // eventBinding();

    // 初始化
    element.init = function(){
        element.maskInput();
        element.eventBinding();
    };

    // 输入框格式化
    element.maskInput = function() {
        // $input_scene_name.inputmask({regex: ".{0,200}"});
        // $input_scene_description.inputmask({regex: ".{0,200}"});
        let maskOption = {
            translation: {
                'Z': {
                    pattern: /./, optional: true
                }
            }
        };
        element.dom.$input_scene_name.mask('Z'.repeat(200), maskOption);  // 限制长度为200
        element.dom.$input_scene_description.mask('Z'.repeat(200), maskOption);  // 限制长度为200
    };

    // 事件绑定
    element.eventBinding = function() {
        // $(document).on('keydown', documentSaveSceneSetting);
        element.dom.$input_scene_name.on('change keyup', function(){
            let scene_name = element.dom.$input_scene_name.val();
            element.dom.$wtd_div_scene_name.children('b').text(scene_name);
        });
        element.dom.$btn_scene_setting_save.on('click', element.saveSceneSetting);
    };

    // 保存测试场景数据
    element.saveSceneSetting = function() {
        let name = element.dom.$input_scene_name.val();
        let description = element.dom.$input_scene_description.val();
        $.ajax({
            type: 'POST',
            url: '/ajax/scene/save',
            data: {
                id: scene_id,
                name: name,
                description: description,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    message('测试场景保存成功', 'success');
                }else{
                    message("测试场景保存失败: " + data.error_msg, "error");
                }
            }
        });
    };

    return element;
}
//
// // 保存测试场景数据
// function saveSceneSetting() {
//     let name = $input_scene_name.val();
//     let description = $input_scene_description.val();
//     $.ajax({
//         type: 'POST',
//         url: '/ajax/scene/save',
//         data: {
//             id: scene_id,
//             name: name,
//             description: description,
//         },
//         success: function (data, status, xhr) {
//             if (data.error_no === 0){
//                 message('测试场景保存成功', 'success');
//             }else{
//                 message("测试场景保存失败: " + data.error_msg, "error");
//             }
//         }
//     });
// }
//
// // ctrl+s保存测试场景数据
// function documentSaveSceneSetting(event) {
//     //可以判断是不是mac，如果是mac,ctrl变为花键
//     if (event.keyCode == 83 && (navigator.platform.match("Mac") ? event.metaKey : event.ctrlKey)) {
//         event.preventDefault();  // 阻止元素事件的默认行为
//         saveSceneSetting();
//     }
// }
