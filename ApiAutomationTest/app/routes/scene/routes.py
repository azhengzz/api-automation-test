# coding=utf-8

from flask import render_template, request, abort
from flask_login import login_required

from app.routes.scene import bp
from app.models import Module, Scene, Project
from app.cores.dictionaries import STATUS, ELEMENT_TYPE, LOGIC_CONTROLLER_TYPE


@bp.route('/')
@login_required
def scene():
    module_id = request.args.get('module_id')
    if module_id is None:
        abort(404)
    module = Module.query.filter_by(id=module_id).first()
    if module is None:
        abort(404)
    projects = Project.query.filter_by(status=STATUS.NORMAL).all()
    return render_template('scene/scene.html', scenes=module.scenes, module=module, project=module.project,
                           projects=projects, ELEMENT_TYPE=ELEMENT_TYPE, LOGIC_CONTROLLER_TYPE=LOGIC_CONTROLLER_TYPE)


@bp.route('/setting/<int:id>')
@login_required
def scene_setting(id):
    scene = Scene.query.filter_by(id=id).first()
    if scene is None:
        abort(404)
    return render_template('scene/scene_setting.html', scene=scene)

