
from flask import Blueprint


bp = Blueprint("scene", __name__)

from app.routes.scene import routes
