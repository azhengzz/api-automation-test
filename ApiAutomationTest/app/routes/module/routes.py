# coding=utf-8

from flask import render_template, request, abort
from flask_login import login_required

from app.routes.module import bp
from app.models import Project


@bp.route('/')
@login_required
def module():
    project_id = request.args.get('project_id')
    if project_id is None:
        abort(404)
    project = Project.query.filter_by(id=project_id).first()
    if project is None:
        abort(404)
    return render_template('module/module.html', project=project)
