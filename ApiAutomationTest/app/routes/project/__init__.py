
from flask import Blueprint


bp = Blueprint("project", __name__)

from app.routes.project import routes
