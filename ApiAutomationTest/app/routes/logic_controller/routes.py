# coding=utf-8

from flask import render_template, abort
from flask_login import login_required

from app.routes.logic_controller import bp
from app.models import LogicController
from app.cores.dictionaries import LOGIC_CONTROLLER_TYPE


@bp.route('/<int:logic_controller_id>')
@login_required
def logic_controller(logic_controller_id):
    if logic_controller_id is None:
        abort(404)
    logic_controller = LogicController.query.filter_by(id=logic_controller_id).first()
    if logic_controller is None:
        abort(404)
    if logic_controller.logic_controller_type == LOGIC_CONTROLLER_TYPE.IF_CONTROLLER:
        if_controller = logic_controller.specific_controller
        return render_template('logic_controller/if.html', if_controller=if_controller)
    elif logic_controller.logic_controller_type == LOGIC_CONTROLLER_TYPE.LOOP_CONTROLLER:
        loop_controller = logic_controller.specific_controller
        return render_template('logic_controller/loop.html', loop_controller=loop_controller)
    elif logic_controller.logic_controller_type == LOGIC_CONTROLLER_TYPE.WHILE_CONTROLLER:
        while_controller = logic_controller.specific_controller
        return render_template('logic_controller/while.html', while_controller=while_controller)
    elif logic_controller.logic_controller_type == LOGIC_CONTROLLER_TYPE.SIMPLE_CONTROLLER:
        simple_controller = logic_controller.specific_controller
        return render_template('logic_controller/simple.html', simple_controller=simple_controller)
    else:
        abort(404)
