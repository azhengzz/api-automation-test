# coding=utf-8

"""
Author: zhangzheng
Description: 供Jinja2使用的全局函数
Version: 0.0.1
LastUpdateDate: 
UpadteURL: 
LOG: 
"""
import json
from typing import Iterable
from flask import url_for, current_app

from app.cores.dictionaries import STATUS
from app.models import Report, SubElementInLogicController, Case, Tool


def render_to_json(rows, columns=None) -> str:
    """
    将表数据转换成由字典组成列表的数据格式
    :param rows: 需要转换的表数据
    :param columns: 需要解析的列
    :return: json字符串
    """
    if columns is None:
        if isinstance(rows, Iterable) and len(rows) > 0:
            row = rows[0]
            if hasattr(row, 'render_field_list'):
                columns = row.render_field_list
            else:
                raise RuntimeError('未指定columns参数')
    l = [{col: getattr(row, col) for col in columns} for row in rows]
    return json.dumps(l)


def sort_by_order_in_logic_controller(logic_controller_id):
    """
    根据logic_controller_id，在SubElementInLogicController表中根据顺序返回组件
    :param logic_controller_id: 逻辑控制器编号
    :return: 该逻辑控制器下的子组件
    """
    sub_elements_in_logic_controller = SubElementInLogicController.query.filter(
        SubElementInLogicController.logic_controller_id == logic_controller_id,
        SubElementInLogicController.order_in_logic_controller > -1,
    ).order_by(
        SubElementInLogicController.order_in_logic_controller.asc()
    ).all()
    return sub_elements_in_logic_controller


def sort_by_order_in_module(scenes):
    """
    根据scene.order_in_module的顺序从小到大排序后返回
    :param scenes: 排序前的测试场景集合
    :return: 排序后的测试场景集合
    """
    scenes.sort(key=lambda scene: scene.order_in_module)
    return scenes


def sort_by_order_in_project(modules):
    """
    根据module.order_in_project的顺序从小到大排序后返回
    :param modules: 排序前模块数据
    :return: 排序后的模块数据
    """
    modules.sort(key=lambda module: module.order_in_project)
    return modules


def can_show(row):
    """根据状态判断是否可以展示在界面上"""
    if row.status in (STATUS.NORMAL, STATUS.FORBIDDEN):
        return True
    else:
        return False


def is_forbidden(row):
    """判断是否是禁用状态"""
    if row.status == STATUS.FORBIDDEN:
        return True
    else:
        return False


def get_latest_reports():
    """获取最近5条报告数据"""
    reports = Report.query.all()
    reports.sort(key=lambda report: report.id, reverse=True)
    return reports[:5]


def get_case_from_id(case_id):
    """
    根据case.id获取case数据库对象
    :param case_id: 案例编号Case.id
    """
    return Case.query.filter_by(id=case_id).first()


def get_tool_from_id(tool_id):
    """
    根据tool.id获取tool数据库对象
    :param tool_id: 工具id编号Tool.id
    """
    return Tool.query.filter_by(id=tool_id).first()


def calc_percent(a, b):
    try:
        a = float(a)
        b = float(b)
    except Exception as e:
        return None
    else:
        if b > 0:
            return round(a / b * 100, 2)
        else:
            return None


def url_for_static(endpoint: str, **values):
    """为静态资源加上版本号"""
    if endpoint.strip().lower() != 'static':
        raise ValueError("endpoint expects a 'static' value")
    values['version'] = current_app.config['AAT_VERSION']
    return url_for(endpoint=endpoint, **values)


if __name__ == '__main__':
    print(render_to_json)
