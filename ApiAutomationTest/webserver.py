# coding=utf-8

import logging

from app import create_app

from app.extensions import socketio


app = create_app()

if __name__ == '__main__':
    # 前置处理
    # 在flask2.2版本后移除了before_first_request方法
    from app.extensions import dispatcher_scheduler
    from app.cores.ws import register_all_user_socket
    with app.app_context():
        dispatcher_scheduler.init_app(app)
        register_all_user_socket()
    host, port = '0.0.0.0', 5000
    debug = False
    log_output = True
    if socketio.async_mode == 'threading':
        socketio.run(app=app, host=host, port=port, debug=debug)
    elif socketio.async_mode == 'eventlet':
        socketio.run(app=app, host=host, port=port, debug=debug, log_output=log_output, log=logging.getLogger())
    elif socketio.async_mode == 'gevent':
        socketio.run(app=app, host=host, port=port, debug=debug, log_output=log_output)
    # app.run(host='0.0.0.0', port=5000)
    # app.run()
