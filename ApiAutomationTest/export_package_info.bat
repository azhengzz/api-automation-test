@echo off
rem encodig=GB2312
echo **********************************************************************
echo ***********************【导出Python环境包信息】***********************
set pip="%cd%\venv\Scripts\pip.exe"
set pipdeptree="%cd%\venv\Scripts\pipdeptree.exe"
rem echo %pip%
if exist %pip% (
    echo ***********************【检测到项目目录下Python解释器】***************
    echo # 项目目录下Python第三方package信息 > %cd%\package_info.txt
    %pip% freeze >> %cd%\package_info.txt
    echo # 项目目录下Python第三方package依赖树信息 > %cd%\package_deptree_info.txt
    %pipdeptree% -a >> %cd%\package_deptree_info.txt
) else (
    echo ***********************【未检测到项目目录下Python解释器，使用系统环境变量目录下Python解释器】************
    echo # 系统环境变量中Python第三方package信息 > %cd%\package_info.txt
    pip freeze >> %cd%\package_info.txt
    echo # 系统环境变量中Python第三方package依赖树信息 > %cd%\package_deptree_info.txt
    pipdeptree -a >> %cd%\package_deptree_info.txt
)
echo ***********************【导出Python包信息完成】***********************
pause